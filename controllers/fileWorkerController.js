import fs from 'fs';
import {emitter} from '../emitter.js';

export const writeSimple = function (req, res) {
    fs.writeFileSync("hello.txt", "Привет!");

    res.send('<h1>Синхронная запись файла...</h1>');
};

export const readSimple = function (req, res) {
    let fileContent = fs.readFileSync("hello.txt", "utf8");

    res.send(`<h1>Синхронное чтение файла...</h1><p>${fileContent}</p>`);
};

export const writeStream = function (req, res) {
    let writeableStream = fs.createWriteStream("stream.txt");
    writeableStream.write("Привет, пользователь!");
    writeableStream.write("Продолжение записи...\n");
    writeableStream.end("Завершение записи.");
    
    res.send('<h1>Потоковая запись в файл...</h1>');
};

export const readStream = function (req, res) {
    let readableStream = fs.createReadStream("stream.txt", "utf8");
    readableStream.on("data", function(chunk){ 
      console.log(chunk);
    });
    
    res.send('<h1>Потоковое чтение файла...</h1>');
};

export const writeFile = function (req, res, next) {
    let fileName = req.file.originalname;
    emitter.emit("writeFile", fileName);

    res.send('ok');
};