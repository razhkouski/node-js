import { Admin } from "../models/admin.js";

export const createAdmin = function (req, res) {
    let admin = new Admin(req.body);
    res.send(`<h1>Регистрация нового администратора</h1>
                <p>admin ID: ${admin.id}</p>
                <p>admin Name: ${admin.name}</p>`);
};

export const getAdminById = function (req, res) {
    res.send(`<p>Получаем администратора с id: ${admin.id} - ${admin.name}</p>`);
};

export const deleteAdminById = function (req, res) {
    res.send(`<p>Удаляем администратора с id: ${admin.id}</p>`);
};

export const updateAdminById = function (req, res) {
    res.send(`<p>Апдейт учётной записи администратора с id: ${admin.id}</p>`);
};