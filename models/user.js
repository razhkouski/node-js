const users = [];

export class User {
    constructor(name, id) {
        this.id = id;
        this.name = name;
    }
    save() {
        users.push(this);
    }
    static getAllUsers() {
        return users;
    }
}