const admins = [];

export class Admin {
    constructor(name, id) {
        this.id = id;
        this.name = name;
    }
    save() {
        admins.push(this);
    }
    static getAllAdmins() {
        return admins;
    }
}