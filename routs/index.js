import userRouter from './user.js';
import adminRouter from './admin.js';
import fileWorker from './fileWorkerRouter.js';

export { userRouter, adminRouter, fileWorker };