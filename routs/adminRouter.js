import express from 'express';
import { adminCreate, getAdminById, deleteAdminById, updateAdminById } from '../controllers/adminController.js';

const adminRouter = express.Router();
const adminParser = express.json();

adminRouter.post("/create", adminParser, adminCreate);
adminRouter.get("/:id", adminParser, getAdminById);
adminRouter.delete("/:id", adminParser, deleteAdminById);
adminRouter.put("/:id", adminParser, updateAdminById);

export default adminRouter;