import express from 'express';
import bodyParser from 'body-parser';
import { userCreate, getUserById, deleteUserById, updateUserById } from '../controllers/userController.js';

const userRouter = express.Router();
const urlencodedParser = bodyParser.urlencoded({extended: true});

userRouter.post("/create", urlencodedParser, userCreate);
userRouter.get("/:id", urlencodedParser, getUserById);
userRouter.delete("/:id", urlencodedParser,deleteUserById);
userRouter.put("/:id", urlencodedParser, updateUserById);

export default userRouter;