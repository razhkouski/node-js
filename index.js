import express from 'express';
import fs from 'fs';
import { emitter } from './emitter.js';
import { userRouter, adminRouter, fileWorker } from './routs/index.js';

const app = express();

app.use('/admin', adminRouter);
app.use('/user', userRouter);
app.use("/fileworker", fileWorker);
app.use("/", function (req, res) {
    res.send(`<h1>Главная страница</h1>`);
});

emitter.on("writeFile", function (fileName) {
    let readableStream = fs.createReadStream(fileName, "utf8");
    let writeableStream = fs.createWriteStream("newFile.txt");
    readableStream.pipe(writeableStream);
});

app.listen(3000);